class Blog
  include Mongoid::Document

  field :title
  field :text
end