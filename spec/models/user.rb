class User
  include Mongoid::Document
  include Mongoid::OptimisticLock

  field :login
  field :email
end