require 'spec_helper'

RSpec.describe 'optimistic_lock' do
  describe User do
    it { is_expected.to be_optimistic_locked_document }
  end

  describe Blog do
    it { is_expected.to_not be_optimistic_locked_document }
  end
end
