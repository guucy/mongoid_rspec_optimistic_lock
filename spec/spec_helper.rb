require 'mongoid_rspec_optimistic_lock'
require 'mongoid'
require 'mongoid/optimistic_lock'
require 'models/user'
require 'models/blog'

RSpec.configure do |config|
  config.include Mongoid::Matchers
end