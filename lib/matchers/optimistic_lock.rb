module Mongoid
  module Matchers
    module OptimisticLock
      RSpec::Matchers.define :be_optimistic_locked_document do
        match do |doc|
          doc.class.included_modules.include?(Mongoid::OptimisticLock)
        end

        description do
          'be a optimistic locked Mongoid document'
        end
      end
    end
  end
end