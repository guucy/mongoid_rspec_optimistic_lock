# MongoidRspecOptimisticLock

Rspec matcher for mongoid_optimistic_lock.

## Status

[![wercker status](https://app.wercker.com/status/956d601c3967bcfa0f5ff4f457f775db/m "wercker status")](https://app.wercker.com/project/bykey/956d601c3967bcfa0f5ff4f457f775db)

## Installation

Add this line to your application's Gemfile:

    gem 'mongoid_rspec_optimistic_lock'

And then execute:

    $ bundle

Drop in existing or dedicated support file in spec/support (spec/support/mongoid.rb)

```ruby
RSpec.configure do |configuration|
  configuration.include Mongoid::Matchers
end
```

## Usage

```ruby
describe User do 
  it { is_expected.to be_optimistic_locked_document }
end
```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
