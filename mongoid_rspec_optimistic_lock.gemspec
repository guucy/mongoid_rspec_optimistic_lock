# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mongoid_rspec_optimistic_lock/version'

Gem::Specification.new do |spec|
  spec.name          = "mongoid_rspec_optimistic_lock"
  spec.version       = Mongoid::Rspec::OptimisticLock::VERSION
  spec.authors       = ["nii_kenichi"]
  spec.email         = ["cm10030@gmail.com"]
  spec.summary       = %q{Rspec matcher for mongoid_optimistic_lock.}
  spec.description   = %q{Rspec matcher for mongoid_optimistic_lock.}
  spec.homepage      = 'https://bitbucket.org/guucy/mongoid_rspec_optimistic_lock'
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split("\n")
  spec.test_files    = `git ls-files -- {spec}/*`.split("\n")
  spec.require_paths = ['lib']

  spec.add_dependency 'rake'
  spec.add_dependency 'mongoid', '~> 4.0'
  spec.add_dependency 'rspec', '~> 3.1'
end
